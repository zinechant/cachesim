#include <stdlib.h>
#include <string>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include "snappy.h"

int main(int argc, char * argv[]){
    bool DoDec = true, DoCom = true, DoDump = true, DoSnappy = true;
    for (int i = 0; i < argc; i++){
        if (0 == strcmp("-NoDec", argv[i]))
            DoDec = false;
        if (0 == strcmp("-NoCom", argv[i]))
            DoCom = false;
        if (0 == strcmp("-NoDump", argv[i]))
            DoDump = false;
        if (0 == strcmp("-NoSnappy", argv[i]))
            DoSnappy = false;
    }

    if (DoCom){
        std::string rootDir(getenv("ROOT"));
        std::string casePath = rootDir +
            "/src/hardware_models/udpsim/cases/snappy_compress/crawl-last";
        std::ifstream inStream(casePath.c_str(), std::ios::binary);
        if (!inStream.is_open()){
            printf("Case %s opened failed\n", casePath.c_str());
            exit(1);
        }
        std::string comInput = std::string((
            std::istreambuf_iterator<char>(inStream)),
            std::istreambuf_iterator<char>());
        inStream.close();
        std::string comOutput("");

        if (DoSnappy)
            snappy::Compress(comInput.data(), comInput.size(), &comOutput);

        printf("Compress: %d -> %d\n",
            (int)comInput.size(), (int)comOutput.size());
        if (DoDump){
            std::ofstream outStream("compressed", std::ios::binary);
            outStream.write(comOutput.c_str(), comOutput.size());
            outStream.close();
        }
    }
    if (DoDec){
        std::ifstream inStream("compressed", std::ios::binary);
        std::string decInput = std::string((
            std::istreambuf_iterator<char>(inStream)),
            std::istreambuf_iterator<char>());
        inStream.close();
        std::string decOutput("");

        if (DoSnappy)
            snappy::Uncompress(decInput.data(), decInput.size(), &decOutput);

        printf("Uncompress: %d -> %d\n",
            (int)decInput.size(), (int)decOutput.size());
        if (DoDump){
            std::ofstream outStream("uncompressed", std::ios::binary);
            outStream.write(decOutput.c_str(), decOutput.size());
            outStream.close();
        }
    }

    return 0;
}
