const int N = 1000;
const int M = 1000;

int main(){
    unsigned int x;
    unsigned int a[N];
    for (int i = 0; i < M; i++)
        for (int j = 0; j < N; j++){
            x += a[j];
            a[j] = x;
        }
    return x;
}