import re
import os
import argparse


def LineGenerator(FilePath):
    FileHD = open(FilePath, 'r')
    while True:
        line = FileHD.readline()
        if not line or line.strip() == '':
            break
        yield line.strip()
    FileHD.close()


def ConvertTrace(TraceFile, ZTraceFile, TFLog):
    TP = os.path.join(os.environ["CSROOT"], "tools/zsim/build/opt/TraceParser")
    os.system("%s %s %s > %s" % (TP, TraceFile, ZTraceFile, TFLog))


def TraceSplit(TraceFile, OutputBase):
    OutputArr = {}
    LineGen = LineGenerator(TraceFile)
    Cycle = 0
    LastAcc = {}

    for line in LineGen:
        LaneId, Category, WordAddr = [int(x) for x in re.match(
            r"MT: ([-\d]+) ([-\d]+) ([-\d]+)", line).groups()]
        if (LaneId == -1 and Category == -1):
            assert(WordAddr > Cycle)
            Cycle = WordAddr
            continue
        assert(LaneId >= 0 and LaneId < 64)
        assert(WordAddr >= 4096 * LaneId)
        if not (LaneId in OutputArr):
            OutputArr[LaneId] = []
        OutputArr[LaneId].append((Cycle, "MT: %d %d %d\n" % (-1, -1, Cycle)))
        OutputArr[LaneId].append((Cycle, "MT: %d %d %d\n" % (
            0, Category, WordAddr - 4096 * LaneId)))
        LastAcc[LaneId] = Cycle

    MaxCycle = LastAcc[min(LastAcc, key=LastAcc.get)]

    for LaneId in OutputArr:
        with open("%s.%02d.trace" % (OutputBase, LaneId), "w") as OutHD:
            for pair in OutputArr[LaneId]:
                if pair[0] > MaxCycle:
                    break
                OutHD.write(pair[1])

        ConvertTrace("%s.%02d.trace" % (OutputBase, LaneId),
                     "%s.%02d.ztrace" % (OutputBase, LaneId),
                     "%s.%02d.log" % (OutputBase, LaneId),)
        print("Output %s.%02d.ztrace" % (OutputBase, LaneId))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Split all memory traces.')
    parser.add_argument("-i", "--InputDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/trace"))
    parser.add_argument("-o", "--OutputDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/trace"))
    args = parser.parse_args()

    InputDir = args.InputDir
    OutputDir = args.OutputDir

    assert(os.path.isdir(InputDir))
    for TraceName in os.listdir(InputDir):
        if (not re.match(r"\w+\.trace$", TraceName)):
            continue
        TraceNamePrefix = re.match(r"(\w+)\.trace$", TraceName).groups()[0]
        print("Splitting %s" % os.path.join(InputDir, TraceName))
        TraceSplit(os.path.join(InputDir, TraceName),
                   os.path.join(OutputDir, TraceNamePrefix))
