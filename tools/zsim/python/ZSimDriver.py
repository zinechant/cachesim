import os
import re
import argparse

from common import MemConfigs
from common import DataOnlyCfg
from common import InstDataCfg

from ConfigWriter import ConfigWriter

ZSimCmd = ""


def ExecZSim(ExecDir, ConfigFile, LogFile):
    assert(os.path.exists(ExecDir))
    assert(os.path.exists(ConfigFile))
    os.chdir(ExecDir)
    print("ExecZSim @ %s" % ExecDir)
    if not os.system("%s %s > %s" % (ZSimCmd, ConfigFile, LogFile)):
        os.remove(LogFile)


def DFSMemConfigs(TraceZSimDir, ExpLevels, Conf, BaseConf, step):
    if (step == len(ExpLevels)):
        if "sys.caches.l2.size" in Conf:
            assert("sys.caches.l1d.size" in Conf)
            l1sizes = Conf["sys.caches.l1d.size"]
            if ("sys.caches.l1i.size" in Conf):
                l1sizes += Conf["sys.caches.l1i.size"]
            if l1sizes >= Conf["sys.caches.l2.size"]:
                return
        name = ""
        for level in ExpLevels:
            name += "%ss%06dw%02d_" % (
                level,
                Conf["sys.caches.%s.size" % level],
                Conf["sys.caches.%s.array.ways" % level]
            )
        ExecDir = os.path.join(TraceZSimDir, name[0: -1])
        if os.path.exists(ExecDir):
            assert(os.path.isdir(ExecDir))
        else:
            os.mkdir(ExecDir)
        ConfigFile = os.path.join(ExecDir, "%s.cfg" % name[0: -1])
        ConfigWriter(BaseConf, Conf, ConfigFile)
        LogFile = os.path.join(ExecDir, "%s.log" % name[0: -1])
        ExecZSim(ExecDir, ConfigFile, LogFile)
    else:
        for currConf in MemConfigs[ExpLevels[step]]:
            Conf["sys.caches.%s.size" % ExpLevels[step]] = currConf["size"]
            Conf["sys.caches.%s.array.ways" % ExpLevels[step]] = currConf[
                "array.ways"]
            DFSMemConfigs(TraceZSimDir, ExpLevels, Conf, BaseConf, step + 1)


def ZSimDriver(TraceZSimDir):
    for u in range(2):
        if (u):
            ExpLevels = ["l1d", "l1i", "l2"]
            InstDataCfg["sys"]["mem"]["latency"] = 1
            DataOnlyCfg["sys"]["mem"]["latency"] = 1
        else:
            ExpLevels = ["l1d", "l1i"]
            InstDataCfg["sys"]["mem"]["latency"] = 0
            DataOnlyCfg["sys"]["mem"]["latency"] = 0
        DFSMemConfigs(TraceZSimDir, ExpLevels, {}, InstDataCfg, 0)
        ExpLevels.remove("l1i")
        DFSMemConfigs(TraceZSimDir, ExpLevels, {}, DataOnlyCfg, 0)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Drive ZSim for different workloads and conf')
    parser.add_argument("-o", "--OutputDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/output"))
    parser.add_argument("-z", "--ZSimCmd", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/build/opt/zsim"))
    parser.add_argument("-t", "--TraceDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/trace"))
    args = parser.parse_args()

    OutputDir = args.OutputDir
    ZSimCmd = args.ZSimCmd
    TraceDir = args.TraceDir

    for TraceFile in os.listdir(TraceDir):
        if (not re.match("^\w+\.\d+\.ztrace$", TraceFile)):
            continue
        TraceZSimDir = os.path.join(OutputDir, TraceFile)
        if os.path.exists(TraceZSimDir):
            assert(os.path.isdir(TraceZSimDir))
        else:
            os.mkdir(TraceZSimDir)
        TraceFile = os.path.join(TraceDir, TraceFile)
        ReTraceFile = re.sub(r"(.*)\.ztrace", r"\1.reztrace", TraceFile)
        print("Driving ZSim for %s" % TraceFile)
        InstDataCfg["sim"]["traceFile"] = TraceFile
        InstDataCfg["sim"]["retraceFile"] = ReTraceFile
        DataOnlyCfg["sim"]["traceFile"] = TraceFile
        DataOnlyCfg["sim"]["retraceFile"] = ReTraceFile

        ZSimDriver(TraceZSimDir)

