import os
import argparse


def MemTrace(RunPath, arg, TracePath):
    os.chdir(RunPath)
    cmd = os.path.join(RunPath, "example ") + arg + " | grep \"MT:\" > %s" %\
        TracePath

    print(cmd)
    fstatus = os.system(cmd)
    if (fstatus):
        print("%s @ %s exists with %d" % (cmd, RunPath, fstatus))
        print("exit gracefully")
        os.exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate all memory traces.')
    parser.add_argument("-s", "--ServiceDir", default=os.path.join(
        os.getenv("ROOT"), "src/libraries/service"))
    parser.add_argument("-t", "--TraceDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/trace"))
    args = parser.parse_args()

    ServiceDir = args.ServiceDir
    TraceDir = args.TraceDir

    workloads = (
        ["snappy", "-NoDec", "SnappyCom.trace"],
        ["snappy", "-NoCom", "SnappyDec.trace"],
        ["huffman", "-NoDec", "HuffCom.trace"],
        ["huffman", "-NoCom", "HuffDec.trace"],
        ["csv_parsing", "", "CsvParsing.trace"],
        ["dictionary_rle", "", "DictRle.trace"],
        ["histogram", "", "Histogram.trace"],
        ["regex", "", "Regex.trace"]
    )
    for workload in workloads:
        workload[0] = os.path.join(ServiceDir, workload[0], "c_stub/example")
        workload[2] = os.path.join(TraceDir, workload[2])
    for workload in workloads:
        MemTrace(workload[0], workload[1], workload[2])
