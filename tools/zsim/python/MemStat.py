from math import log
import re
import os
import argparse

from common import MemConfigs


def CactiConfig(Conf, BaseConfFile, OutConfFile):
    repls = []
    if BaseConfFile[-6:] == "lm.cfg":
        repls.append([r"^-UCA\s*bank\s*count\s*\d+\s*$",
                     "-UCA bank count %d\n" % (Conf["size"]/16384)])

    repls.append([r"^-size\s*\(bytes\)\s*\d+\s*$",
                 "-size (bytes) %d\n" % Conf["size"]])
    repls.append([r"^-associativity\s*\d+\s*$",
                 "-associativity %d\n" % Conf["array.ways"]])
    repls.append([r"^-tag\s+size\s+\(b\)\s*\d+\s*$", "-tag size (b) %d\n" % (
                 24 - int(0.9 + log(Conf["size"]/Conf["array.ways"], 2)))])

    with open(BaseConfFile, "r") as InHD:
        with open(OutConfFile, "w") as OutHD:
            for line in InHD:
                for repl in repls:
                    line = re.sub(repl[0], repl[1], line)
                OutHD.write(line)


def MemStat(MemDir, CactiCmd):
    for MemLevel in MemConfigs:
        BaseConfig = os.path.join(MemDir, "%s.cfg" % MemLevel)
        for Conf in MemConfigs[MemLevel]:
            OutConfFile = os.path.join(MemDir, "%s_s%06dw%02d.cfg" % (
                MemLevel, Conf["size"], Conf["array.ways"]))
            CactiConfig(Conf, BaseConfig, OutConfFile)
            OutCactiFile = os.path.join(MemDir, "%s_s%06dw%02d.cacti" % (
                MemLevel, Conf["size"], Conf["array.ways"]))
            os.system("%s -infile %s > %s" % (
                CactiCmd, OutConfFile, OutCactiFile))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Generate Statistics for different Memory Configurations')
    parser.add_argument("-i", "--MemDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/mem"))
    parser.add_argument("-c", "--CactiCmd", default=os.path.join(
        os.getenv("CSROOT"), "tools/cacti65/cacti"))
    args = parser.parse_args()

    MemDir = args.MemDir
    CactiCmd = args.CactiCmd

    MemStat(MemDir, CactiCmd)
