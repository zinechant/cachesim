import argparse
import os
import re
import json

from common import Workloads
from ZSimParser import ZSimParser
from common import MemConfigs
from common import LogicTime


def AggregateCalc(MemJson, ZSimOutputDir, TraceDir):

    AllData = {}
    for workload in Workloads:
        assert(workload not in AllData)
        AllData[workload] = {}
        FirstTraceFile = True
        for TraceFile in os.listdir(TraceDir):
            if not re.match(r"%s\.\d+\.ztrace$" % workload, TraceFile):
                continue

            KTrace = re.match(r"%s\.(\d+)\.ztrace$" % workload,
                              TraceFile).groups()[0]
            PerTraceDir = os.path.join(ZSimOutputDir, TraceFile)
            assert(os.path.isdir(PerTraceDir))

            FirstConf = True

            for Conf in os.listdir(PerTraceDir):
                if FirstTraceFile:
                    assert(Conf not in AllData[workload])
                    AllData[workload][Conf] = {}
                confs = Conf.split("_")
                for i in range(len(confs)):
                    confs[i] = re.sub("l1d", "l1d_", confs[i])
                    confs[i] = re.sub("l1i", "l1i_", confs[i])
                    confs[i] = re.sub("l2", "l2_", confs[i])
                    assert(confs[i] in MemJson)
                TraceConfDir = os.path.join(PerTraceDir, Conf)

                print("Processing %s" % TraceConfDir)

                ZSimOutput = os.path.join(TraceConfDir, "zsim.out")
                assert(os.path.exists(ZSimOutput))

                MemLevels = ["driver0", "driver1", "l1d"]
                if re.match(".*_l1i", Conf):
                    MemLevels.append("l1i")
                if re.match(".*_l2", Conf):
                    MemLevels.append("l2")
                JsonFile = os.path.join(TraceConfDir, "zsim.json")
                data = ZSimParser(ZSimOutput, MemLevels, JsonFile)
                assert("Cycles" not in data)
                if "driver1" in data:
                    data["Cycles"] = max(data["driver0"]["cycles"],
                                         data["driver1"]["cycles"])
                else:
                    data["Cycles"] = data["driver0"]["cycles"]
                MemLevels.remove("driver0")
                MemLevels.remove("driver1")
                ConfDict = {}
                for i in range(len(MemLevels)):
                    ConfDict[MemLevels[i]] = confs[i]
                l1acctime = 0
                CacheLeakagePower = 0
                CacheDynamicEnergy = 0
                for level in MemLevels:
                    data[level]["Leakage power (mW)"] = (
                        MemJson[ConfDict[level]]["Leakage power (mW)"])
                    data[level]["Read energy per access (nJ)"] = (MemJson[
                        ConfDict[level]]["Read energy per access (nJ)"])
                    data[level]["Access time (ns)"] = (
                        MemJson[ConfDict[level]]["Access time (ns)"])

                    CacheLeakagePower += (
                        MemJson[ConfDict[level]]["Leakage power (mW)"])
                    CacheDynamicEnergy += (
                        (data[level]["hGETS"] + data[level]["hGETX"] +
                         data[level]["mGETS"] + data[level]["mGETXIM"] +
                         data[level]['mGETXSM']) *
                        MemJson[ConfDict[level]][
                            "Read energy per access (nJ)"]
                    )

                    if re.match("l1", level):
                        l1acctime = max(
                            l1acctime,
                            MemJson[ConfDict[level]]["Access time (ns)"]
                        )

                CycleTime = l1acctime + LogicTime
                data["Cycle time (ns)"] = CycleTime
                data["Time (ns)"] = CycleTime * data["Cycles"]

                lastLevel = MemLevels[-1]
                lastLevelMisses = (data[lastLevel]["mGETS"] +
                                   data[lastLevel]["mGETXIM"] +
                                   data[lastLevel]['mGETXSM'])
                if lastLevel == "l1i":
                    lastLevelMisses += (data["l1d"]["mGETS"] +
                                        data["l1d"]["mGETXIM"] +
                                        data["l1d"]['mGETXSM'])

                TotalAccesses = 0
                for level in ("l1i", "l1d"):
                    if level in data:
                        TotalAccesses += (
                            data[level]["hGETS"] + data[level]["hGETX"] +
                            data[level]["mGETS"] + data[level]["mGETXIM"] +
                            data[level]['mGETXSM']
                        )

                data["Cache leakage power (mW)"] = CacheLeakagePower
                data["Cache leakage energy (nJ)"] = (CacheLeakagePower / 1000 *
                                                     data["Time (ns)"])
                data["Cache dynamic power (mW)"] = (CacheDynamicEnergy *
                                                    1000 / data["Time (ns)"])
                data["Cache dynamic energy (nJ)"] = CacheDynamicEnergy

                data["Global cache hit rate"] = (
                    1 - lastLevelMisses / TotalAccesses)

                data["Leakage power (mW)"] = {}
                data["Dynamic power (mW)"] = {}
                data["Total power (mW)"] = {}
                data["Leakage energy (nJ)"] = {}
                data["Dynamic energy (nJ)"] = {}
                data["Total energy (nJ)"] = {}

                for LMConf in MemConfigs["lm"]:
                    ConfName = "lm_s%06dw%02d" % (
                        LMConf["size"], LMConf["array.ways"]
                    )
                    data["Leakage power (mW)"][ConfName] = (
                        CacheLeakagePower +
                        MemJson[ConfName]["Leakage power (mW)"]
                    )
                    data["Dynamic energy (nJ)"][ConfName] = (
                        CacheDynamicEnergy + lastLevelMisses *
                        MemJson[ConfName]["Read energy per access (nJ)"]
                    )
                    data["Dynamic power (mW)"][ConfName] = (
                        data["Dynamic energy (nJ)"][ConfName] * 1000 /
                        data["Time (ns)"]
                    )
                    data["Total power (mW)"][ConfName] = (
                        data["Dynamic power (mW)"][ConfName] +
                        data["Leakage power (mW)"][ConfName]
                    )
                    data["Leakage energy (nJ)"][ConfName] =(
                        data["Leakage power (mW)"][ConfName] *
                        data["Time (ns)"] / 1000
                    )
                    data["Total energy (nJ)"][ConfName] = (
                        data["Total power (mW)"][ConfName] *
                        data["Time (ns)"] / 1000
                    )

                assert(KTrace not in AllData[workload][Conf])
                AllData[workload][Conf][KTrace] = data
                if FirstConf:
                    for LMConf in MemConfigs["lm"]:
                        ConfName = "lm_s%06dw%02d" % (
                            LMConf["size"], LMConf["array.ways"]
                        )
                        lmdata = {}
                        lmdata["Total accesses"] = TotalAccesses
                        CycleTime = LogicTime + MemJson[ConfName][
                            "Access time (ns)"]
                        lmdata["Cycle time (ns)"] = CycleTime
                        lmdata["Cycles"] = (data["Cycles"] -
                                            data["driver0"]["skew"])
                        lmdata["Time (ns)"] = (lmdata["Cycle time (ns)"] *
                                               lmdata["Cycles"])
                        lmdata["Cache leakage power (mW)"] = 0
                        lmdata["Cache dynamic energy (nJ)"] = 0
                        lmdata["Cache leakage energy (nJ)"] = 0
                        lmdata["Cache dynamic power (mW)"] = 0
                        lmdata["Leakage power (mW)"] = (
                            MemJson[ConfName]["Leakage power (mW)"]
                        )
                        lmdata["Dynamic energy (nJ)"] = (
                            TotalAccesses *
                            MemJson[ConfName]["Read energy per access (nJ)"]
                        )
                        lmdata["Dynamic power (mW)"] = (
                            lmdata["Dynamic energy (nJ)"] * 1000 /
                            lmdata["Time (ns)"]
                        )
                        lmdata["Total power (mW)"] = (
                            lmdata["Dynamic power (mW)"] +
                            lmdata["Leakage power (mW)"]
                        )
                        lmdata["Leakage energy (nJ)"] = (
                            lmdata["Leakage power (mW)"] *
                            lmdata["Time (ns)"] / 1000
                        )
                        lmdata["Total energy (nJ)"] = (
                            lmdata["Total power (mW)"] *
                            lmdata["Time (ns)"] / 1000
                        )
                        ConfName = re.sub("lm_", "lm", ConfName)
                        if FirstTraceFile:
                            assert(ConfName not in AllData[workload])
                            AllData[workload][ConfName] = {}
                        assert(KTrace not in AllData[workload][ConfName])
                        AllData[workload][ConfName][KTrace] = lmdata

                FirstConf = False
            FirstTraceFile = False

    return AllData


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Aggregate ZSim Results and Calculate Power Numbers')
    parser.add_argument("-m", "--MemJsonFile", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/mem/All.json"))
    parser.add_argument("-z", "--ZSimOutputDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/output"))
    parser.add_argument("-t", "--TraceDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/trace"))
    parser.add_argument("-a", "--AllJsonFile", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/output/AllData.json"))
    parser.add_argument("-r", "--Redo", action='store_true',
                        help="Redo the calculation")

    args = parser.parse_args()

    with open(args.MemJsonFile, "r") as InHD:
        MemJson = json.load(InHD)

    ZSimOutputDir = args.ZSimOutputDir
    TraceDir = args.TraceDir
    AllJsonFile = args.AllJsonFile
    Redo = args.Redo

    if Redo or not os.path.exists(AllJsonFile):
        AllData = AggregateCalc(MemJson, ZSimOutputDir, TraceDir)
        with open(AllJsonFile, "w") as OutHD:
            json.dump(AllData, OutHD, sort_keys=True, indent=2)
    else:
        with open(AllJsonFile, "r") as InHD:
            AllData = json.load(InHD)

    OutDir = os.path.split(AllJsonFile)[0]

    for workload in Workloads:
        with open(os.path.join(OutDir, "%s.json" % workload), "w") as OutHD:
            json.dump(AllData[workload], OutHD, sort_keys=True, indent=2)
