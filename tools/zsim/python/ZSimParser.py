import re
import os
import json


def ZSimParser(ZSimOutFile, MemLevels, JsonFile):
    data = {}
    with open(ZSimOutFile, "r") as InHD:
        currSec = ""
        for line in InHD:
            for nWhites in range(len(line)):
                if line[nWhites] != ' ':
                    break
            if nWhites == 1:
                grp = re.match(r"^\s(\w+):\s.*$", line).groups()
                assert(len(grp) == 1)
                currSec = grp[0]
                if (currSec in MemLevels):
                    assert(currSec not in data)
                    data[currSec] = {}
            elif nWhites == 2:
                if (currSec[:6] == "driver"):
                    grp = re.match(r"^\s\schild-(\d):\s.*$", line).groups()
                    assert(len(grp) == 1)
                    currSec = "driver" + str(grp[0])
                    assert(currSec in MemLevels)
                    assert(currSec not in data)
                    data[currSec] = {}
            elif nWhites == 3:
                if (currSec in MemLevels):
                    grp = re.match(r"^\s\s\s(\w+):\s(\d+)\s#\s.*$",
                                   line).groups()
                    assert(len(grp) == 2)
                    assert(currSec in data)
                    data[currSec][grp[0]] = int(grp[1])
    if JsonFile is not None:
        with open(JsonFile, "w") as OutHD:
            json.dump(data, OutHD, sort_keys=True, indent=4)
    else:
        if __name__ == "__main__":
            print(json.dumps(data, sort_keys=True, indent=4))
    return data

if __name__ == "__main__":
    ExecDir = ("/home/zinechant/CacheSim/tools/zsim/output/"
               "SnappyDec.00.ztrace/l1ds004096w01_l1is002048w02")
    MemLevels = ("driver0", "driver1", "l1d", "l1i")
    ZSimParser(os.path.join(ExecDir, "zsim.out"), MemLevels,
               os.path.join(ExecDir, "zsim.json"))
