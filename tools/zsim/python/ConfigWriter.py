import os
import argparse
import copy

from common import InstDataCfg


def RecursivePrint(pdict, OutHD, level):
    for key in sorted(pdict):
        whites = ''.join([' ' for i in range(4*level)])
        if type(pdict[key]) is dict:
            OutHD.write("%s%s = {\n" % (whites, key))
            RecursivePrint(pdict[key], OutHD, level+1)
            OutHD.write("%s};\n" % whites)
        else:
            assert(type(pdict[key]) is int or
                   type(pdict[key]) is bool or
                   type(pdict[key]) is str)
            if type(pdict[key]) is str:
                OutHD.write('%s%s = "%s";\n' % (whites, key, pdict[key]))
            else:
                OutHD.write('%s%s = %s;\n' % (whites, key, pdict[key]))


def ConfigWriter(DefConf, config, OutFilePath):
    NewConf = copy.deepcopy(DefConf)
    for key in config:
        assert(type(config[key]) is not dict)
        keys = key.split('.')
        p = NewConf
        for q in keys[0: -1]:
            assert(type(p) is dict)
            assert(q in p)
            p = p[q]
        assert(type(p) is dict)
        p[keys[-1]] = config[key]

    with open(OutFilePath, "w") as OutHD:
        RecursivePrint(NewConf, OutHD, 0)

if __name__ == "__main__":
    ConfigWriter(
        InstDataCfg,
        {"sys.caches.l1i.size": 2048, "sys.caches.l1i.latency": 2},
        os.path.join(os.getenv("CSROOT"), "tools/zsim/cfg/DefConfig.cfg")
    )
