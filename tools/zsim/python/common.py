import copy
from math import log

LogicTime = 0.8

l1 = []
for i in (1, 2, 4, 8):
    for j in [(1 << k) for k in range(1 + int(log(i, 2)))]:
        l1.append({"size": 1024*i, "array.ways": j})

l2 = [{"size": 4096*i, "array.ways": j}
      for i in (1, 2, 4) for j in (1, 2, 4, 8)]
l2.remove({"size": 4096, "array.ways": 8})


MemConfigs = {
    "l1d": copy.deepcopy(l1),
    "l1i": copy.deepcopy(l1),
    "l2": copy.deepcopy(l2),
    "lm": [{"size": 16384*i, "array.ways": 1} for i in (1, 2, 4, 8)]
}

Workloads = ("Regex", "Histogram", "DictRle", "CsvParsing",
             "HuffCom", "HuffDec", "SnappyCom", "SnappyDec")

LMSizeNeeded ={
    "Regex": 16384,
    "Histogram": 16384,
    "DictRle": 16384,
    "CsvParsing": 32768,
    "HuffCom": 16384,
    "HuffDec": 16384,
    "SnappyCom": 131072,
    "SnappyDec": 131072,
}

InstDataCfg = {
    "sys": {
        "caches": {
            "l0d": {
                "caches": 1,
                "type": "TraceDriven",
            },
            "l0i": {
                "caches": 1,
                "type": "TraceDriven",
            },
            "l1d": {
                "caches": 1,
                "size": 8192,
                "latency": 0,
                "children": "l0d",
                "array": {
                    "ways": 1,
                    "type": "SetAssoc",
                },
            },
            "l1i": {
                "caches": 1,
                "size": 8192,
                "latency": 0,
                "children": "l0i",
                "array": {
                    "ways": 1,
                    "type": "SetAssoc",
                },
            },
            "l2": {
                "caches": 1,
                "size": 131072,
                "latency": 1,
                "children": "l1d|l1i",
                "array": {
                    "ways": 1,
                    "type": "SetAssoc",
                },
            },
        },
        "mem": {
            "controllers": 1,
            "type": "Simple",
            "latency": 1,
        },
    },

    "sim": {
        "printHierarchy": True,
        "traceDriven": True,
        "useSkews": True,
        "playPuts": True,
        "playAllGets": True,
        "traceFile":
            "/home/zinechant/CacheSim/tools/zsim/trace/SnappyCom.0.ztrace",
        "retraceFile":
            "/home/zinechant/CacheSim/tools/zsim/trace/SnappyCom.0.retrace",
        "phaseLength": 10000,
    },

    "process0": {
        "command": "time",
    },
}

DataOnlyCfg = {
    "sys": {
        "caches": {
            "l0d": {
                "caches": 1,
                "type": "TraceDriven",
            },
            "l1d": {
                "caches": 1,
                "size": 8192,
                "latency": 0,
                "children": "l0d",
                "array": {
                    "ways": 1,
                    "type": "SetAssoc",
                },
            },
            "l2": {
                "caches": 1,
                "size": 131072,
                "latency": 1,
                "children": "l1d",
                "array": {
                    "ways": 1,
                    "type": "SetAssoc",
                },
            },
        },
        "mem": {
            "controllers": 1,
            "type": "Simple",
            "latency": 1,
        },
    },

    "sim": {
        "printHierarchy": True,
        "traceDriven": True,
        "useSkews": True,
        "playPuts": True,
        "playAllGets": True,
        "traceFile":
            "/home/zinechant/CacheSim/tools/zsim/trace/SnappyCom.0.ztrace",
        "retraceFile":
            "/home/zinechant/CacheSim/tools/zsim/trace/SnappyCom.0.retrace",
        "phaseLength": 10000,
    },

    "process0": {
        "command": "time",
    },
}

AvgEqItem = [
    "Cycle time (ns)"
]

AvgItem = [
    "Cycles",
    "Cycle time (ns)",
    "Time (ns)",
    "Cache leakage power (mW)",
    "Cache leakage energy (nJ)",
    "Cache dynamic power (mW)",
    "Cache dynamic energy (nJ)",
]
AvgLmItem = [
    "Leakage power (mW)",
    "Dynamic power (mW)",
    "Total power (mW)",
    "Leakage energy (nJ)",
    "Dynamic energy (nJ)",
    "Total energy (nJ)",
]
