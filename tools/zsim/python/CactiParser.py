import re
import os
import argparse
import json

from common import MemConfigs

ReMatch = (
    (r"\s+Access\s*time\s*\(ns\):\s*([\d\.]+)", "Access time (ns)", float),
    (r"Associativity\s*:\s*(\d+)", "Associativity", int),
    (r"Block\s*size\s*:\s*(\d+)", "Block Size", int),
    (r"Cache\s*size\s*:\s*(\d+)", "Cache Size", int),
    (r"\s+Cache\s*height\s*x\s*width\s*\(mm\):\s*([\d\.]+)\s*x\s*[\d\.]+",
        "Cache height (mm)", float),
    (r"cache\s*type\s*:\s*(\w+)", "Cache type", str),
    (r"\s+Cache\s*height\s*x\s*width\s*\(mm\):\s*[\d\.]+\s*x\s*([\d\.]+)",
        "Cache width (mm)", float),
    (r"\s+Cycle\s*time\s*\(ns\):\s*([\d\.]+)", "Cycle time (ns)", float),
    (r"\s+Total\s*leakage\s*power\s*of\s*a\s*bank\s*\(mW\):\s*([\d\.]+)",
        "Leakage power (mW)", float),
    (r"\s+Total\s*dynamic\s*read\s*energy\s*per\s*access\s*\(nJ\):\s*([\d\.]+)",
        "Read energy per access (nJ)", float),
    (r"Read\s*only\s*ports\s*:\s*(\d+)", "Read only ports", int),
    (r"Read\s*write\s*ports\s*:\s*(\d+)", "Read write ports", int),
    (r"Single\s*ended\s*read\s*ports\s*:\s*(\d+)",
        "Single ended read ports", int),
    (r"Tag\s*size\s*:\s*(\d+)", "Tag size", int),
    (r"Technology\s*:\s*([\d\.]+)", "Technology", float),
    (r"Temperature\s*:\s*(\d+)", "Temperature", int),
    (r"Write\s*only\s*ports\s*:\s*(\d+)", "Write only ports", int),
)


def CactiParser(CactiFile, JsonFile):
    data = {}
    with open(CactiFile, "r") as CFHD:
        for line in CFHD:
            for reexp in ReMatch:
                if (re.match(reexp[0], line)):
                    grp = re.match(reexp[0], line).groups()
                    assert(len(grp) == 1)
                    if (reexp[2] is int):
                        data[reexp[1]] = int(grp[0])
                    elif (reexp[2] is float):
                        data[reexp[1]] = float(grp[0])
                    elif (reexp[2] is str):
                        data[reexp[1]] = str(grp[0])
                    else:
                        assert(0)
    with open(JsonFile, "w") as OutHD:
        OutHD.write(json.dumps(data, sort_keys=True, indent=4))
    return data

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Parse and Extract from Cacti Output to Json')
    parser.add_argument("-i", "--MemDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/mem"))
    parser.add_argument("-a", "--AllJsonFile", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/mem/All.json"))
    parser.add_argument("-l", "--LogFile", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/mem/All.log"))

    args = parser.parse_args()

    MemDir = args.MemDir
    AllJsonFile = args.AllJsonFile
    LogFile = args.LogFile

    LogHD = open(LogFile, "w")

    All = {}

    for MemL in sorted(MemConfigs.keys()):
        for Conf in MemConfigs[MemL]:
            CactiFile = os.path.join(MemDir, "%s_s%06dw%02d.cacti" % (
                MemL, Conf["size"], Conf["array.ways"]))
            assert(os.path.exists(CactiFile))
            JsonFile = os.path.join(MemDir, "%s_s%06dw%02d.json" % (
                MemL, Conf["size"], Conf["array.ways"]))
            ConfName = "%s_s%06dw%02d" % (MemL, Conf["size"], Conf["array.ways"])
            All[ConfName] = CactiParser(CactiFile, JsonFile)
            LogHD.write("%s\t%d\t%d\t%f\t%f\t%f\n" % (
                MemL, Conf["size"], Conf["array.ways"],
                All[ConfName]["Access time (ns)"],
                All[ConfName]["Read energy per access (nJ)"],
                All[ConfName]["Leakage power (mW)"]))

    LogHD.close()
    with open(AllJsonFile, "w") as OutHD:
        json.dump(All, OutHD, sort_keys=True, indent=4)

