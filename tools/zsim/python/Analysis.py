import argparse
import os
import json
import statistics
import re
import numpy
import matplotlib.pyplot as plt


from common import Workloads
from common import MemConfigs
from common import AvgEqItem
from common import AvgItem
from common import AvgLmItem
from common import LMSizeNeeded


def AverageTrace(AllData):
    AvgData = {}
    StdData = {}
    for workload in AllData:
        assert(workload in Workloads)
        AvgData[workload] = {}
        StdData[workload] = {}
        for conf in AllData[workload]:
            if conf[:2] == "lm":
                avg = {}
                AvgData[workload][conf] = {}
                StdData[workload][conf] = {}
                AvgData[workload][conf]["NTrace"] = (
                    len(AllData[workload][conf]))
                StdData[workload][conf]["NTrace"] = (
                    len(AllData[workload][conf]))
                for item in AvgItem + AvgLmItem + ["Total accesses"]:
                    avg[item] = []
                for trace in AllData[workload][conf]:
                    for item in AvgItem + AvgLmItem + ["Total accesses"]:
                        avg[item].append(AllData[workload][conf][trace][item])
                for item in AvgItem + AvgLmItem + ["Total accesses"]:
                    assert(len(avg[item]) == AvgData[workload][conf]["NTrace"])
                    AvgData[workload][conf][item] = (
                        sum(avg[item]) / len(avg[item]))
                    if len(avg[item]) == 1:
                        StdData[workload][conf][item] = 0
                    elif AvgData[workload][conf][item] == 0:
                        StdData[workload][conf][item] = 0
                    else:
                        StdData[workload][conf][item] = (
                            statistics.stdev(avg[item]) /
                            AvgData[workload][conf][item])
            else:
                AvgData[workload][conf] = {}
                StdData[workload][conf] = {}
                AvgData[workload][conf]["NTrace"] = (
                    len(AllData[workload][conf]))
                StdData[workload][conf]["NTrace"] = (
                    len(AllData[workload][conf]))
                avg = {}
                for item in AvgItem + ["Global cache hit rate"]:
                    avg[item] = []
                for item in AvgLmItem:
                    avg[item] = {}
                    for lmconf in MemConfigs["lm"]:
                        lmname = "lm_s%06dw%02d" % (lmconf["size"],
                                                    lmconf["array.ways"])
                        avg[item][lmname] = []
                for trace in AllData[workload][conf]:
                    for item in AvgItem + ["Global cache hit rate"]:
                        avg[item].append(AllData[workload][conf][trace][item])
                    for item in AvgLmItem:
                        for lmconf in MemConfigs["lm"]:
                            lmname = "lm_s%06dw%02d" % (lmconf["size"],
                                                        lmconf["array.ways"])
                            avg[item][lmname].append(
                                AllData[workload][conf][trace][item][lmname])
                for item in AvgEqItem:
                    assert(abs(max(avg[item]) - min(avg[item])) < 1e-3)
                for item in AvgItem + ["Global cache hit rate"]:
                    assert(len(avg[item]) == AvgData[workload][conf]["NTrace"])
                    AvgData[workload][conf][item] = (
                        sum(avg[item]) / len(avg[item]))
                    if len(avg[item]) == 1:
                        StdData[workload][conf][item] = 0
                    else:
                        StdData[workload][conf][item] = (
                            statistics.stdev(avg[item]) /
                            AvgData[workload][conf][item])

                for item in AvgLmItem:
                    AvgData[workload][conf][item] = {}
                    StdData[workload][conf][item] = {}
                    for lmconf in MemConfigs["lm"]:
                        lmname = "lm_s%06dw%02d" % (lmconf["size"],
                                                    lmconf["array.ways"])
                        assert(len(avg[item][lmname]) ==
                               AvgData[workload][conf]["NTrace"])
                        AvgData[workload][conf][item][lmname] = (
                            sum(avg[item][lmname]) / len(avg[item][lmname]))
                        if len(avg[item][lmname]) == 1:
                            StdData[workload][conf][item] = 0
                        else:
                            StdData[workload][conf][item] = (
                                statistics.stdev(avg[item][lmname]) /
                                AvgData[workload][conf][item][lmname])
    return AvgData, StdData


def LevelPlot(AvgData, FileRE):
    fig, axes = plt.subplots(4, 2, figsize=(8, 10))
    for i in range(len(AvgData.keys())):
        workload = sorted(list(AvgData.keys()))[i]
        ax = axes[i // 2][i % 2]
        energies = []
        times = []
        nLevels = []
        lmName = "lms%06dw01" % LMSizeNeeded[workload]
        lm_Name = "lm_s%06dw01" % LMSizeNeeded[workload]

        for conf in AvgData[workload]:
            if (conf[:2] == "lm"):
                continue
            nLevels.append(len(re.findall("_", conf)))
            if nLevels[-1] == 1:
                if re.findall("l2", conf):
                    nLevels[-1] = 2
            elif nLevels[-1] == 2:
                nLevels[-1] = 3
            times.append(AvgData[workload][conf]["Time (ns)"])

            energies.append(AvgData[workload][conf][
                "Total energy (nJ)"][lm_Name])
        energies = numpy.array(energies)
        times = numpy.array(times)
        nLevels = numpy.array(nLevels)

        ax.scatter(times[nLevels == 0], energies[nLevels == 0], c="r",
                   label="l1")
        ax.scatter(times[nLevels == 1], energies[nLevels == 1], c="b",
                   label="l1d+l1i")
        ax.scatter(times[nLevels == 2], energies[nLevels == 2], c="g",
                   label="l1d+l2")
        ax.scatter(times[nLevels == 3], energies[nLevels == 3], c="k",
                   label="l1d+l1i+l2")
        ax.axvline(AvgData[workload][lmName]["Time (ns)"], c="c")
        ax.axhline(AvgData[workload][lmName]["Total energy (nJ)"],
                   c="c")
        ax.legend(loc=4)

        ax.set(xlabel='Time (ns)', ylabel='Total energy (nJ)',
               title="(%d) %s" % (i, workload))
        ax.set_ylim(ymin=0)
        plt.tight_layout()
    fig.savefig(FileRE % "LevelPlot", transparent=True)


def BestETP(AvgData, BETPLog):
    BestData = {}
    for workload in AvgData:
        lmName = "lms%06dw01" % LMSizeNeeded[workload]
        lm_Name = "lm_s%06dw01" % LMSizeNeeded[workload]
        BETP = 1 << 63

        BestData[workload] = {}
        for conf in AvgData[workload]:
            if (conf[:2] == "lm"):
                continue
            ETP = (AvgData[workload][conf]["Total energy (nJ)"][lm_Name] *
                   AvgData[workload][conf]["Time (ns)"])
            if ETP < BETP:
                BestData[workload] = {}
                BData = AvgData[workload][conf]
                for item in AvgLmItem:
                    BData[item] = BData[item][lm_Name]
                BestData[workload][conf] = BData
                BETP = ETP
        BestData[workload][lmName] = AvgData[workload][lmName]

    with open(BETPLog, "w") as OutHD:
        for workload in sorted(BestData.keys()):
            for conf in sorted(BestData[workload].keys()):
                if conf[:2] != "lm":
                    OutHD.write("%s\t" % workload)
                    OutHD.write("%s_%s\t" % tuple(
                        sorted(BestData[workload].keys())))
                    OutHD.write("%f\t" % BestData[workload][conf][
                        "Global cache hit rate"])
                OutHD.write("%f\t" % BestData[workload][conf]["Time (ns)"])
                OutHD.write("%f\t" % BestData[workload][conf][
                    "Total energy (nJ)"])
                OutHD.write("%f\t" % BestData[workload][conf][
                    "Dynamic energy (nJ)"])
                OutHD.write("%f\t" % BestData[workload][conf][
                    "Leakage energy (nJ)"])
            OutHD.write("\n")

    return BestData


def SysPerf(BETPData, FigRE):
    SysData = {}
    x = (1, 2, 4, 8)
    Perf = {}
    Power = {}

    for i in x:
        SysData[i] = {}
        LMSize = i * 16384
        for workload in Workloads:
            if i == 1:
                Perf[workload] = []
                Power[workload] = []
            SysData[i][workload] = {}
            CacheName, LMName = tuple(sorted(BETPData[workload].keys()))
            LanePerEx = max(LMSizeNeeded[workload], LMSize) / LMSize
            ExecUnit =  64 / LanePerEx
            perf = (ExecUnit * BETPData[workload][LMName]["Time (ns)"] /
                    BETPData[workload][CacheName]["Time (ns)"])
            Perf[workload].append(perf)
            SysData[i][workload]["RPerf"] = perf
            power = (
                ExecUnit * BETPData[workload][CacheName]["Total power (mW)"] +
                (64 - ExecUnit) * (
                    BETPData[workload][CacheName]["Cache dynamic power (mW)"] +
                    BETPData[workload][CacheName]["Cache leakage power (mW)"]
                ) + max(0, (LMSize / LMSizeNeeded[workload]) - 1) * 64 *
                BETPData[workload][LMName]["Leakage power (mW)"]
            ) / BETPData[workload][LMName]["Total power (mW)"]
            SysData[i][workload]["RPower"] = power
            Power[workload].append(power)

    fig, ax = plt.subplots(figsize=(6, 6))
    for workload in Workloads:
        ax.plot(x, Perf[workload], label=workload, linestyle='--', marker='o')
    ax.legend()
    ax.set(xlabel='Relative Local Memory Size', ylabel='Relative Performance')
    fig.savefig(FigRE % ("Perf"))

    fig, ax = plt.subplots(figsize=(6, 6))
    for workload in Workloads:
        ax.plot(x, Power[workload], label=workload, linestyle='--', marker='o')
    ax.legend()
    ax.set(xlabel='Relative Local Memory Size', ylabel='Relative Power')
    fig.savefig(FigRE % ("Power"))

    PerfOPower = {}
    for workload in Workloads:
        PerfOPower[workload] = []
        for i in range(len(x)):
            PerfOPower[workload].append(Perf[workload][i]/Power[workload][i])
    fig, ax = plt.subplots(figsize=(6, 6))
    for workload in Workloads:
        ax.plot(x, PerfOPower[workload], label=workload,
                linestyle="--", marker='o')
    ax.set(xlabel='Relative Local Memory Size',
           ylabel='Relative Performance / Relative Power')
    ax.legend()
    fig.savefig(FigRE % ("PerfOPower"))

    return SysData

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Data analysis and draw plots')
    parser.add_argument("-a", "--AllJsonFile", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/output/AllData.json"))
    parser.add_argument("-g", "--AvgJsonFile", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/output/AvgData.json"))
    parser.add_argument("-d", "--StdJsonFile", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/output/StdData.json"))
    parser.add_argument("-s", "--SysDataFile", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/output/SysData.json"))
    parser.add_argument("-b", "--BestJsonFile", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/output/BestData.json"))
    parser.add_argument("-r", "--Redo", action='store_true',
                        help="Redo the calculation")
    parser.add_argument("-o", "--OutputDir", default=os.path.join(
        os.getenv("CSROOT"), "tools/zsim/output"))

    args = parser.parse_args()

    AllJsonFile = args.AllJsonFile
    AvgJsonFile = args.AvgJsonFile
    StdJsonFile = args.StdJsonFile
    BestJsonFile = args.BestJsonFile
    SysDataFile = args.SysDataFile
    Redo = args.Redo
    OutputDir = args.OutputDir

    if Redo or not os.path.exists(AvgJsonFile):
        with open(AllJsonFile, "r") as InHD:
            AllData = json.load(InHD)

        AvgData, StdData = AverageTrace(AllData)

        with open(AvgJsonFile, "w") as OutHD:
            json.dump(AvgData, OutHD, sort_keys=True, indent=2)
        with open(StdJsonFile, "w") as OutHD:
            json.dump(StdData, OutHD, sort_keys=True, indent=2)

        FileRE = os.path.join(OutputDir, "%s.%s.json")

        for workload in Workloads:
            with open(FileRE % (workload, "avg"), "w") as OHD:
                json.dump(AvgData[workload], OHD, sort_keys=True, indent=2)
            with open(FileRE % (workload, "std"), "w") as OHD:
                json.dump(StdData[workload], OHD, sort_keys=True, indent=2)
    else:
        with open(AvgJsonFile, "r") as IHD:
            AvgData = json.load(IHD)
    LevelPlot(AvgData, os.path.join(OutputDir, "LevelPlot.%s.png"))
    BETPLog = os.path.join(OutputDir, "BETP.log")
    BETPData = BestETP(AvgData, BETPLog)
    with open(BestJsonFile, "w") as OHD:
        json.dump(BETPData, OHD, sort_keys=True, indent=2)

    FigRE = os.path.join(OutputDir, "%s.png")
    SysData = SysPerf(BETPData, FigRE)
    with open(SysDataFile, "w") as OutHD:
        json.dump(SysData, OutHD, sort_keys=True, indent=2)
