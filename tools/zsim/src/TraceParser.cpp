#include "access_tracing.h"
#include "galloc.h"
#include "memory_hierarchy.h"  // to translate access type to strings
#include "stdio.h"
#include "assert.h"
#include "cstdint"

void print_acc(const AccessRecord & acc){
    printf("AccessRecord{lineAddr = %lu, reqCycle = %lu, latency = %d, childId = %d, type = %d}\n",
        (uint64_t)acc.lineAddr, acc.reqCycle, acc.latency, acc.childId, (int)acc.type);
}


int main(int argc, char *argv[]){
    if (argc != 3) {
        printf("Program to parse UdpSim trace to ZSim trace.\n");
        printf("  Usage: %s UdpSimTracePath ZSimTracePath\n", argv[0]);
        exit(1);
    }

    bool inst[9*16384>>6] = {false};
    bool data[9*16384>>6] = {false};

    gm_init(32<<20 /*32 MB --- should be enough*/);

    FILE *UdpSimT = fopen(argv[1], "r");
    if (UdpSimT == nullptr){
        printf("%s not opened successfully", argv[1]);
        exit(1);
    }
    unsigned int Cycle = 0;

    char c1, c2, c3;
    int Category, LaneId, WordAddr;
    AccessTraceWriter* ZSimT = new AccessTraceWriter(argv[2], 2);

    while (EOF != fscanf(UdpSimT, "%c%c%c %d %d %d\n",
                         &c1, &c2, &c3, &LaneId, &Category, &WordAddr)){
        if (c1 != 'M' || c2 != 'T' || c3 != ':') continue;
        if (LaneId == -1 && Category == -1){
            assert(WordAddr >= 0);
            assert((unsigned int) WordAddr >= Cycle);
            Cycle = WordAddr;
            continue;
        }
        assert(LaneId == 0);
        assert(WordAddr >= 0 || WordAddr < 9*4096);
        AccessRecord acc =  {(Address)(WordAddr >> 4), Cycle, 0, 0,
            (Category & 1) ? GETX : GETS};
        if (inst[acc.lineAddr]) acc.childId = 1;
        else if (data[acc.lineAddr]) acc.childId = 0;
             else acc.childId = uint32_t(Category >> 1);

        print_acc(acc);

        ZSimT->write(acc);
        if (acc.childId){
            assert(!data[acc.lineAddr]);
            inst[acc.lineAddr] = true;
        } else {
            assert(!inst[acc.lineAddr]);
            data[acc.lineAddr] = true;
        }
    }
    fclose(UdpSimT);
    ZSimT->dump(false);
    delete(ZSimT);

    return 0;
}