#include "Cache.h"
#include <string>
#include <cstdint>
#include <cassert>
#include <cstdio>
uint32_t GNofLanes = 64;

int main(int argc, char *argv[]){
    assert(argc == 2);
    FILE *Trace = fopen(argv[1], "r");
    if (Trace == NULL){
        printf("Open File %s Failed", argv[1]);
        return 1;
    }

    CacheSim::Cache* L3 = new CacheSim::Cache(8192, 16, std::string("LRU"), NULL);
    CacheSim::Cache** L2s = new CacheSim::Cache*[GNofLanes];
    CacheSim::Cache** L1Is = new CacheSim::Cache*[GNofLanes];
    CacheSim::Cache** L1Ds = new CacheSim::Cache*[GNofLanes];

    for (uint32_t i = 0; i < GNofLanes; i++){
        L2s[i] = new CacheSim::Cache(16, 4, std::string("LRU"), L3);
        L1Is[i] = new CacheSim::Cache(4, 2, std::string("LRU"), L2s[i]);
        L1Ds[i] = new CacheSim::Cache(4, 2, std::string("LRU"), L2s[i]);
    }

    while (!feof(Trace)){
        int c;
        if (EOF == (c = fgetc(Trace))){
            break;
        }
        if (c != 'M'){
            while (c != '\n'){
                if (EOF == (c = fgetc(Trace)))
                    break;
            }
            continue;
        }
        if (EOF == (c = fgetc(Trace))){
            break;
        }
        if (c != 'T'){
            while (c != '\n'){
                if (EOF == (c = fgetc(Trace)))
                    break;
            }
            continue;
        }
        if (EOF == (c = fgetc(Trace))){
            break;
        }
        if (c != ':'){
            while (c != '\n'){
                if (EOF == (c = fgetc(Trace)))
                    break;
            }
            continue;
        }
        uint32_t laneid, cat, add;
        fscanf(Trace, " %d %d %d\n", &laneid, &cat, &add);
        assert((cat <= 2) && (cat >= 0));
        assert((laneid >= 0) && (laneid < GNofLanes));
        assert((add < (1 << 12)) || ((add >> 12) == laneid));

        add = (add & ((1 << 12) - 1)) + (laneid << 12);
        if (cat)
            L1Is[laneid]->Access(add >> 4, cat);
        else
            L1Ds[laneid]->Access(add >> 4, cat);
    }
    uint64_t L1IH = 0;
    uint64_t L1IM = 0;
    uint64_t L1DH = 0;
    uint64_t L1DM = 0;
    uint64_t L2H = 0;
    uint64_t L2M = 0;


    for (uint32_t i = 0; i < GNofLanes; i++){
        printf("L1I[%d]:   Hit %lu    Miss %lu\n",
            i, L1Is[i]->getHitNum(), L1Is[i]->getMissNum());
        printf("L1D[%d]:   Hit %lu    Miss %lu\n",
            i, L1Ds[i]->getHitNum(), L1Ds[i]->getMissNum());
        printf("L2 [%d]:   Hit %lu    Miss %lu\n",
            i, L2s[i]->getHitNum(), L2s[i]->getMissNum());
        L1IH += L1Is[i]->getHitNum();
        L1IM += L1Is[i]->getMissNum();
        L1DH += L1Ds[i]->getHitNum();
        L1DM += L1Ds[i]->getMissNum();
        L2H += L2s[i]->getHitNum();
        L2M += L2s[i]->getMissNum();
    }
    printf("\nL1I:  Hit %lu    Miss %lu\n", L1IH, L1IM);
    printf("\nL1D:  Hit %lu    Miss %lu\n", L1DH, L1DM);
    printf("\nL2:  Hit %lu    Miss %lu\n", L2H, L2M);

    printf("\nL3:  Hit %lu    Miss %lu\n",
        L3->getHitNum(), L3->getMissNum());
}
