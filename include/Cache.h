#ifndef CACHE_H
#define CACHE_H

#include <cstdint>
#include "CacheSet.h"
namespace CacheSim{

class Cache{
private:
    uint32_t const NSet;
    uint32_t const NAss;
    // uint32_t const NISet;
    // uint32_t const NIAss;
    uint64_t misses;
    uint64_t hits;
    Cache* const NextLevel;
    CacheSet **Sets;
    // CacheSet **ISets;

public:
    Cache(uint32_t NSet, uint32_t NAss, const std::string & ReplAlgoName,
        Cache* const NextLevel) : NSet(NSet), NAss(NAss),
            // NISet(0), NIAss(0),
            NextLevel(NextLevel){
        misses = 0;
        hits = 0;
        // ISets = NULL;
        Sets = new CacheSet*[NSet];
        for (uint32_t i = 0; i < NSet; i++)
            Sets[i] = new CacheSet(NAss, ReplAlgoName);
    }

    // Cache(uint32_t NSet, uint32_t NAss, uint32_t NISet, uint32_t NIAss,
    //     std::string const & ReplAlgoName, Cache* const NextLevel) :
    //         NSet(NSet), NAss(NAss), NISet(NISet), NIAss(NIAss),
    //             NextLevel(NextLevel){
    //     misses = 0;
    //     hits = 0;
    //     Sets = new CacheSet*[NSet];
    //     for (uint32_t i = 0; i < NSet; i++)
    //         Sets[i] = new CacheSet(NAss, ReplAlgoName, this);
    //     ISets = new CacheSet*[NISet];
    //     for (uint32_t i = 0; i < NISet; i++)
    //         ISets[i] = new CacheSet(NIAss, ReplAlgoName, this);
    // }

    ~Cache(){
        for (uint32_t i = 0; i < NSet; i++)
            free(Sets[i]);
        free(Sets);
    }

    uint64_t getMissNum(){
        return misses;
    }
    uint64_t getHitNum(){
        return hits;
    }

    void Reset(){
        misses = 0;
        hits = 0;
        for (uint32_t i = 0; i < NSet; i++)
            Sets[i]->Reset();
    }

    void Access(uint32_t CLAdd, uint32_t Cat){
        // if ((0 != Cat) && (NULL != ISets) && (0 != NISet) && (0 != NIAss)){
        //     ISets[CLAdd % NISet]->Access(CLAdd, Cat);
        // } else{
        if (Sets[CLAdd % NSet]->Access(CLAdd, Cat)){
            misses++;
            FetchFromNextLevel(CLAdd, Cat);
        } else{
            hits++;
        }


        // }
    }

    void FetchFromNextLevel(uint32_t CLAdd, uint32_t Cat){
        if (NextLevel != NULL)
            NextLevel->Access(CLAdd, Cat);
    }
};


}

#endif