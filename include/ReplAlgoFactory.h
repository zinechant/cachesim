#ifndef REPL_ALGO_FACTORY_H
#define REPL_ALGO_FACTORY_H

#include "ReplAlgo.h"
#include "LRU.h"
#include <cstdint>
#include <cassert>
#include <string>

namespace CacheSim{

ReplAlgo *ReplAlgoFactory(const std::string &ReplAlgoName, uint32_t SetSize){
    ReplAlgo *ans = NULL;
    if (ReplAlgoName == "LRU"){
        ans = (ReplAlgo*) new LRU(SetSize);
    } else {
        bool unDefinedReplAlgo = false;
        assert(unDefinedReplAlgo);
    }
    return ans;
};

};

#endif