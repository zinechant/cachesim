#ifndef REPL_ALGO_H
#define REPL_ALGO_H

#include <cstdint>
namespace CacheSim{

class ReplAlgo{
public:
    // uint32_t const SetSize;
    //The set is indexed by [SetSize - 1].

    virtual void Access(uint32_t id) = 0;
    virtual uint32_t ToBeErased() = 0;
    virtual void Erase(uint32_t ElemId) = 0;
    virtual void Reset() = 0;

};

};

#endif
