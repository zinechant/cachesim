#ifndef LRU_H
#define LRU_H

#include "ReplAlgo.h"
#include <cstdint>
#include <cassert>

namespace CacheSim{

class LRU : public ReplAlgo{
private:
    uint64_t stamp;
    uint64_t *lastUsed;

public:
    uint32_t const SetSize;
    LRU(uint32_t SetSize): SetSize(SetSize) {
        lastUsed = new uint64_t[SetSize];
        for (uint32_t i = 0; i < SetSize; i++){
            lastUsed[i] = 0;
        }
        stamp = 0;
    }

    void Access(uint32_t id){
        assert(id >= 0);
        assert(id < SetSize);
        lastUsed[id] = ++stamp;
    }

    uint32_t ToBeErased(){
        uint32_t id = 0;
        for (uint32_t i = 0; i < SetSize; i++)
            if (lastUsed[i] < lastUsed[id]) id = i;
        return id;
    }

    void Erase(uint32_t ElemId){
        lastUsed[ElemId] = 0;
    }

    void Reset(){
        for (uint32_t i = 0; i < SetSize; i++){
            lastUsed[i] = 0;
        }
    }

};


};

#endif