#ifndef CACHESET_H
#define CACHESET_H

#include "ReplAlgoFactory.h"
#include <cstdint>
#include <string>
#include <queue>
#include <cassert>

namespace CacheSim{


class CacheSet {
private:
    ReplAlgo* replalgo;
    uint32_t* array;
    bool* taken;
    std::queue<uint32_t> vacancy;

public:
    uint32_t const SetSize;

    CacheSet(uint32_t SetSize, const std::string & ReplAlgoName) :
            SetSize(SetSize) {
        array = new uint32_t[SetSize];
        taken = new bool[SetSize];
        replalgo = ReplAlgoFactory(ReplAlgoName, SetSize);
        for (uint32_t id = 0; id < SetSize; id++){
            taken[id] = false;
            vacancy.push(id);
        }
    }

    bool Access(uint32_t CLAdd, uint32_t cat){
        bool miss = false;
        uint32_t EntryId = SetSize;
        for (uint32_t i = 0; i < SetSize; i++){
            if (array[i] == CLAdd){
                EntryId = i;
            }
        }
        if (EntryId == SetSize){
            miss = true;
        }
        if (EntryId == SetSize && !vacancy.empty()){
            EntryId = vacancy.front();
            taken[EntryId] = true;
            vacancy.pop();
        }
        if (EntryId == SetSize){
            EntryId = replalgo->ToBeErased();
            assert(taken[EntryId]);
            replalgo->Erase(EntryId);
        }
        assert(EntryId < SetSize);
        replalgo->Access(EntryId);
        array[EntryId] = CLAdd;
        return miss;
    }

    void Reset(){
        replalgo->Reset();
        while(!vacancy.empty())
            vacancy.pop();
        for (uint32_t id = 0; id < SetSize; id++){
            taken[id] = false;
            vacancy.push(id);
        }
    }

};

};

#endif