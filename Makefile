INCLUDE = ./include
CXXFLAGS = -std=c++11 -g -O0 -Wall -I$(INCLUDE)
SRCS = $(shell find . -not -path "./src" -name "*.cpp")
OBJS = $(SRCS:.cpp=.o)
TARGET = build/sim

SHARED =
LINK =

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) $(SHARED) -o $(TARGET) $(OBJS) $(LINK)

.PHONY: clean
clean:
	rm -rf *~ .*~ */*~ */.*~ $(OBJS) $(TARGET)
